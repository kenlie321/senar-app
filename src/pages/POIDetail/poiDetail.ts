import { Component, ChangeDetectorRef } from '@angular/core';
import { 
	NavController, 
	NavParams,
	AlertController, 
    Platform, 
    ToastController, 
    LoadingController,
} from 'ionic-angular';
import { PromoDetail } from '../PromoDetail/promoDetail';
import { BuildingMap } from '../BuildingMap/buildingMap';

declare var cordova: any;

@Component({
  selector: 'poi-detail',
  templateUrl: 'poiDetail.html'
})

export class POIDetail{
	public building: any;
    public poi: any;
		public promoList: any[] = [];
		public isEvent:any;
		public isMall:any;

    constructor(
    	public NavController:NavController, 
    	public NavParams:NavParams,
    	public LoadingController: LoadingController,
    	public ChangeDetectorRef: ChangeDetectorRef,
    	public ToastController: ToastController,
    	public Platform: Platform,
    ){
    	this.poi = NavParams.get('poi');
			this.building = NavParams.get('building');
			this.isEvent = NavParams.get('isEvent');
			this.isMall = NavParams.get('isMall');
    }

    ionViewDidEnter(){
    	this.Platform.ready().then(() => {
			this.fetchEventsFromBuilding(this.building);
		}).catch(error =>{
			console.log("error",error);
		});
    }

    fetchEventsFromBuilding(building){
		let loading = null;
		if(this.promoList.length == 0){
			loading = this.LoadingController.create({content:'Loading Promos...'});
			loading.present();
		}
		cordova.plugins.Situm.fetchEventsFromBuilding(building,(result: any) => {
			console.log('promo', result);   
			let promos = result;
			if (promos.length == 0) {
			  this.hideLoading(loading);
			  const message = 'This building has no Events';
			  this.presentToast(message, 'bottom', null);
			  return;
			}
			this.promoList = promos.filter(promo => {
				return promo.customFields.organizer == this.poi.poiName;
			});
			console.log(this.promoList);
			this.hideLoading(loading);
			this.ChangeDetectorRef.detectChanges();
		}, (error) => {
			const errorMsg = 'An error occurred when recovering Promos/Events.' 
			console.log(`${errorMsg}`, error);
			if(loading) loading.dismiss();
			this.presentToast(`${errorMsg} ${error}`, 'bottom', null);
		});
    };

    private hideLoading(loading) {
		if (typeof loading != undefined && typeof loading != null ) {
			loading.dismissAll();
			loading = null;
		}
    }

    presentToast(text, position, toastClass) {
		const toast = this.ToastController.create({
			message: text,
			duration: 3000,
			position: position,
			cssClass: toastClass ? toastClass : ''
		});
		toast.present();
    }

    promoTapped(event, promo){
      this.NavController.push(PromoDetail, {promo: promo,building:this.building,isEvent:this.isEvent,isMall:this.isMall,poi:this.poi});
		}
		
		mapTapped(event){
			this.NavController.push(BuildingMap,{poi:this.poi,building:this.building});
		}
}