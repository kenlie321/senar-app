import { Component, ChangeDetectorRef } from '@angular/core';
import { 
	NavController, 
	NavParams, 
	AlertController, 
    Platform, 
    ToastController, 
    LoadingController,
} from 'ionic-angular';

import { POIList } from '../POIList/poi';

import{ BuildingMap } from '../BuildingMap/buildingMap';

declare var cordova: any;

@Component({
  selector: 'category-list',
  templateUrl: 'categories.html'
})

export class CategoryList{
	public building: any;
    public categoryList: any[] = [];
    
    isEvent:any;
    isMall:any;

    constructor(
    	public NavController: NavController, 
    	public NavParams: NavParams,
    	public LoadingController: LoadingController,
    	public ChangeDetectorRef: ChangeDetectorRef, 
    	public ToastController: ToastController,
    ){
    	  this.building = NavParams.get('building');
        this.isEvent = NavParams.get('isEvent');
        this.isMall = NavParams.get('isMall');
    }
    
    ionViewDidEnter(){
      this.fetchPoisCategoryList();
    }

    fetchPoisCategoryList() {
    	let loading = null;
    	if(this.categoryList.length == 0){
            loading = this.LoadingController.create({
            	content: 'Loading category list...'
            });
            loading.present();
    	}
    	cordova.plugins.Situm.fetchPoiCategories((result) => {
    		console.log('poi cat', result)
    		if(this.isEvent){
                this.categoryList = result.filter((category) => {
                  return category.poiCategoryCode.includes('evt');
                });
              }else{
                this.categoryList = result.filter((category) => {
                  return category.poiCategoryCode.includes('mall');
                });
              }
    		this.ChangeDetectorRef.detectChanges();
    		loading.dismiss();
    	}, (error) => {
    		console.log(error)
    		const errorMessage = "An error occurred when recovering the categories.";
            console.log(`${errorMessage}`, error);
            if(loading) loading.dismiss();
            this.presentToast(`${errorMessage} ${error}`, 'bottom', null);
    	})
    }

    showBuildingMap(event){
      this.NavController.push(BuildingMap, {building: this.building});
    }

    presentToast(text, position, toastClass) {
        const toast = this.ToastController.create({
            message: text,
            duration: 3000,
            position: position,
            cssClass: toastClass ? toastClass : ''
        });
        toast.present();
    }

    showImage(icon_selected){
    	var icon = 'https://dashboard.situm.es' + icon_selected;
    	console.log(icon)
    }

    categoryTapped(event, building, category){
      this.NavController.push(POIList, {building: building, category: category,isEvent:this.isEvent,isMall:this.isMall});
    }
}