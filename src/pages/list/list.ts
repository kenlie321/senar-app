import { Component, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import {LocalNotifications} from '@ionic-native/local-notifications';

import {SelectionPage} from '../selection/selection';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;
  txt:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private diagnostic:Diagnostic,
    private localNotif:LocalNotifications) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');
    this.txt = this.navParams.get('txt');
    // Let's populate this page with some filler content for funzies
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];
    for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(ListPage, {
      item: item
    });
  }

  cobaDiagnostic(event){
    let successCallback = (isAvailable) => { this.txt = isAvailable };
    let errorCallback = (e) => {this.txt = e};

  //   this.diagnostic.isLocationEnabled().then(successCallback).catch(errorCallback);
  this.diagnostic.requestLocationAuthorization().then(successCallback).catch(errorCallback);
    
  }

  cobaNotif(){
    this.localNotif.schedule({
      id:1,
      text:'test notif',
      title:"Test",
      icon:"https://i.imgur.com/fwVsoP7.png",
      launch:true,
      trigger:{at:new Date()},
      // actions:[
      //   { id: 'yes', title: 'Yes' },
      //   { id: 'no',  title: 'No' }
      // ]
      actions:[{
        id:'1',
        
      }]
    });

    this.localNotif.on("yes").subscribe(() => {
      this.navCtrl.setRoot(SelectionPage);
    });
    this.localNotif.on("no").subscribe(() => {
      console.log("test");
    });
  }

  cek(){
    console.log(this.txt);
  }
}
