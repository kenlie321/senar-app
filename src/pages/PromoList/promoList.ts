import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, Platform, ToastController, LoadingController } from 'ionic-angular';

import { PromoDetail } from '../PromoDetail/promoDetail';

declare var cordova: any;

@Component({
  selector: 'promo-list',
  templateUrl: 'promoList.html'
})

export class PromoList{
    building: any;
    promos: any[] = [];
    isEvent:any;
    isMall:any;

    constructor(public navCtrl:NavController, public navParam:NavParams,
      public platform: Platform, public toastCtrl:ToastController,
      public loadCtrl:LoadingController, public detector: ChangeDetectorRef){
        
      this.building = this.navParam.get('building');
      this.isEvent = this.navParam.get('isEvent');
      this.isMall = this.navParam.get('isMall');
    }

    ionViewDidEnter(){
      this.platform.ready().then(()=>{
        this.fetchEventsFromBuilding(this.building);
      }).catch(error =>{
        console.log("error",error);
      });
    }

    fetchEventsFromBuilding(building){
      let loading = null;
      if(this.promos.length == 0){
        loading = this.loadCtrl.create({content:'Loading Promos...'});
        loading.present();
      }
      cordova.plugins.Situm.fetchEventsFromBuilding(building,(result: any) => {
        //console.log('promo', result);   
        this.promos = result;
        if (this.promos.length == 0) {
          this.hideLoading(loading);
          const message = 'This building has no Events';
          this.presentToast(message, 'bottom', null);
          return;
        }
        this.hideLoading(loading);
        this.detector.detectChanges();
      }, (error) => {
        const errorMsg = 'An error occurred when recovering Promos/Events.' 
        console.log(`${errorMsg}`, error);
        if(loading) loading.dismiss();
        this.presentToast(`${errorMsg} ${error}`, 'bottom', null);
      });
    };

    promoTapped(event,promo){
      this.navCtrl.push(PromoDetail,{promo:promo,building:this.building,isEvent:this.isEvent,isMall:this.isMall});
    }
    
    cek(){
      console.log(this.building);
    }

    private hideLoading(loading) {
      if (typeof loading != undefined && typeof loading != null ) {
        loading.dismissAll();
        loading = null;
      }
    }

    presentToast(text, position, toastClass) {
      const toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position: position,
        cssClass: toastClass ? toastClass : ''
      });
      toast.present();
    }
}