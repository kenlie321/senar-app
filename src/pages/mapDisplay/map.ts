import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, Platform, LoadingController, ToastController, AlertController, Button } from 'ionic-angular';
import { GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    CameraPosition,
    MarkerOptions,
    Marker,
    MarkerCluster,
    GroundOverlay,
    GroundOverlayOptions, 
    MyLocation,
    GoogleMapsAnimation,
    ILatLng,
    MarkerIcon,
    Polyline,
    PolylineOptions,
    Spherical,
    } from '@ionic-native/google-maps';
import { Diagnostic } from '@ionic-native/diagnostic';
import { PermissionsService } from '../../services/permissions';
import { SocialSharing } from '@ionic-native/social-sharing';

declare var cordova: any;

const ROUTE_COLOR = '#000000';

// Positioning parameters
const defaultOptionsMap = {
    useDeadReckoning: false,
    interval: 2000,
    indoorProvider: 'INPHONE',
    useBle: true,
    useWifi: true, 
    motionMode: 'BY_FOOT',
    useForegroundService: true,
    outdoorLocationOptions: {
      continuousMode: true,
      userDefinedThreshold: false,
      burstInterval: 1,
      averageSnrThreshold: 25.0
    },
    beaconFilters: [],
    smallestDisplacement: 1.0,
    realtimeUpdateInterval: 2000
  };

@Component({
  selector: 'map-display',
  templateUrl: 'map.html'
})

export class MapDisplay{
    building:any;
    floors:any[] = [];
    pois:any[] = [];
    poiCategories:any[] = [];
    currentFloor:any;
    promo:any;
    selectedPoi:any;

    nearestPoi:any;

    isInsideEvent:boolean;

    //spherical: Spherical;
    
    notShowAgain:boolean = false;

    positioning:boolean = false;

    position:any = {
        statusName:'',
        floorIdentifier:'',
        x:-1,
        y:-1,
        accuracy:-1,
        bearing:''
    }

    currentFloorTxt:any;

    txt:string="";
    route:any;

    polyline:Polyline;

    accessible:boolean = false;

    navigating:boolean = false;

    map:GoogleMap;
    marker:Marker;
    markerPoi = [];
    overlay:GroundOverlay;

    constructor(public navCtrl: NavController, public navParams:NavParams, 
        public detector:ChangeDetectorRef, public loadCtrl:LoadingController,
        public toastCtrl:ToastController, public platform: Platform,
        private diagnostic:Diagnostic, public alertCtrl:AlertController,
        private permissions:PermissionsService, public socialSharing:SocialSharing)
    {
        this.building = this.navParams.get("building");
        this.promo = this.navParams.get('promo');
        this.selectedPoi = this.navParams.get('poi');
    }

    ionViewDidEnter(){
        this.platform.ready().then(() => {
            this.showMap();
            this.platform.pause.subscribe(()=>{
                if(this.positioning){
                    this.stopPositioning(null);
                }
            });
        }).catch(error => {
            console.log('error',error);
        });
    }

    ionViewWillLeave(){
        this.stopPositioning(null);
    }

    showMap(){
        if (!this.map) {
              // Shows a loading while the map is not displayed
              let loading = this.createLoading('Loading map...');
              loading.present();
              // Fetchs all floors of a building
              
              cordova.plugins.Situm.fetchFloorsFromBuilding(this.building, (res) => {
                this.floors = res;
                this.currentFloor = res[0];
                //if(loading) loading.dismiss();
                this.detector.detectChanges();
                this.mountMap();
                
                this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
                    this.mountOverlay(loading);
                  }).catch((err: any) =>  this.handleError(err, loading));
              });
        }
    }

    mountMap(){
        //console.log(this.building);
        //console.log('promo map',this.promo);
        let mapOptions: GoogleMapOptions = {
            camera:{
                target:{
                    lat:this.building.center.latitude,
                    lng:this.building.center.longitude
                },
                zoom:17,
                tilt:10
            },
            controls:{
                compass:true,
                indoorPicker:false
            },
            styles:[
                {
                    featureType: "poi",
                    //elementType: "labels",
                    stylers: [
                          { visibility: "off" }
                    ]
                }
            ]
        };
        this.map = GoogleMaps.create('map-canvas', mapOptions);
        // console.log(this.map.get);
    }

    private mountOverlay(loading) {
        //console.log(this.building.bounds);
        let bounds = this.getBounds(this.building);
        let bearing = (this.building.rotation * 180 / Math.PI);
        // console.log(bounds);
        // console.log(bearing);
        // console.log(this.currentFloor.mapUrl);
        let groundOptions: GroundOverlayOptions = {
          url: this.currentFloor.mapUrl,
          bounds: bounds,
          bearing: bearing,
          anchor: [0.5, 0.5]
        }
        console.log(groundOptions);
        this.map.addGroundOverlay(groundOptions).then((overlay:GroundOverlay) => {
            this.fetchPoiList(this.building,loading);
            this.overlay = overlay;
        }).catch((err: any) => this.handleError(err, loading));
      }

    private drawMarker(loading){
        let markerPosition:ILatLng;
        if(this.promo){
            markerPosition = {
                lat:this.promo.trigger.center.coordinate.latitude,
                lng:this.promo.trigger.center.coordinate.longitude
            };
        }else{
            markerPosition = {
                lat:this.selectedPoi.coordinate.latitude,
                lng:this.selectedPoi.coordinate.longitude
            }
        }
        let icon: MarkerIcon = {
            size: {
                height: 35,
                width: 35
            }
        }
        let markerOptions: MarkerOptions = {
            icon: icon,
            position: markerPosition,
            title: this.promo ? this.promo.name : this.selectedPoi.poiName,
            };
            this.createMarker(markerOptions,this.map,false);
            this.startPositioning(loading);
        }  
    
    getLocationTapped(event){
        this.diagnostic.isLocationEnabled().then((isEnabled)=>{
            if(!isEnabled){
                console.log(isEnabled);
                const activateLocation = this.alertCtrl.create({
                    title:"Enable Location",
                    message:"You Must Enable Location to use this Service",
                    buttons:[{
                        text:"Dissmiss",
                        handler:()=>{
                            this.txt = "You Must Enable Location";
                            return;
                        }
                    },{
                        text:"Enable",
                        handler:()=>{
                            return this.diagnostic.switchToLocationSettings();
                        }
                    }]
                });
                activateLocation.present();
            }else{
                //Get user location
                this.map.getMyLocation().then((location:MyLocation)=>{
                    console.log(JSON.stringify(location,null,2));
                    //move to marker
                    this.map.animateCamera({
                        target:location.latLng,
                        zoom:18,
                        tilt:20
                    }).then(()=>{
                        //add marker
                        let marker: Marker = this.map.addMarkerSync({
                            title:"Your Location",
                            position:location.latLng,
                            animation:GoogleMapsAnimation.BOUNCE
                        });
                        marker.showInfoWindow();
                    });
                });
            }
        }).catch(error=>{
            console.log('error',error);
        })
    }

    private fetchPoiList(building,loading){
        cordova.plugins.Situm.fetchIndoorPOIsFromBuilding(building,(res:any)=>{
            console.log("pois awal",res);
            this.pois = res;
            if (this.pois.length == 0) {
                this.hideLoading(loading);
                const message = 'This building has no POIs';
                this.presentToast(message, 'bottom', null);
                return;
              }
            this.detector.detectChanges();
            this.fetchForPOICategories(building,loading);
        });
    }

    private fetchForPOICategories(building,loading){
        cordova.plugins.Situm.fetchPoiCategories((res:any)=>{
            this.poiCategories = res;
            this.drawPOIsOnMap(this.pois);
            if(!this.promo && !this.selectedPoi){
                this.hideLoading(loading);
            }else if(this.promo){
                this.drawMarker(loading);
            }else{
                this.startPositioning(loading);
            }
        })
    }

    private drawPOIsOnMap(pois){
        pois.forEach(poi => {
            var notShow = {notShowAgain:false,findCategoryAgain:false};
            // poi = {...poi,...notShow};
            if(!poi.findCategoryAgain){
                Object.assign(poi,notShow);
            }
            if(poi.findCategoryAgain == false){
                poi.category = this.findPOICategory(poi);
            }
            let markerPosition: ILatLng = {
              lat: poi.coordinate.latitude,
              lng: poi.coordinate.longitude
            }
            let icon: MarkerIcon = {
              size: {
                height: 20,
                width: 20,
              },
              //url: "https://dashboard.situm.es" + poi.category.icon_selected,
            }
            if (poi.category) icon.url = "https://dashboard.situm.es" + poi.category.icon_selected;
      
            let markerOptions: MarkerOptions = {
              icon: icon,
              position: markerPosition,
              title: `${poi.poiName}`,
            };
            // this.createMarker(markerOptions, this.map, false);
            this.map.addMarker(markerOptions).then((marker:Marker) => {
                this.markerPoi.push(marker);
            });
          });
          console.log('pois',this.pois);
    }

    private findPOICategory(poi) {
        return this.poiCategories.find((poiCategory: any) => {
          return poiCategory.poiCategoryCode == poi.category
        });
      }
    
    private startPositioning(loading){
        this.permissions.checkLocationPermissions().then((permission)=>{
            console.log('location permission',permission);
            if(this.marker) this.marker.remove();
            if(permission){
                this.diagnostic.isLocationEnabled().then((isEnabled) => {
                    if(isEnabled){
                        if(this.positioning == true){
                            const message = "Posistioning is Enabled";
                            this.presentToast(message,'bottom',null);
                            return;
                        }
                        if(!this.map){
                            const message = "Map is not Available";
                            this.presentToast(message,'bottom',null);
                            return;
                        }
                        if(loading == null){
                            loading = this.createLoading("Positioning...");
                            loading.present();
                        }
                        this.createPositionMarker();
                        const locationOptions = this.mountLocationOptions();
                        console.log(locationOptions);
                        console.log(this.position);
                        console.log(this.marker);
                        //set callback and start listen to onLocationChanged Event
                        cordova.plugins.Situm.startPositioning(locationOptions,(res:any) => {
                            this.positioning = true;
                            this.position = res;
                            if(this.currentFloor.floorIdentifier !== res.floorIdentifier){
                                this.floors.forEach(floor => {
                                    if(floor.floorIdentifier == res.floorIdentifier){
                                        this.currentFloor = floor;
                                        this.currentFloorTxt = this.currentFloor.floorIdentifier
                                    }
                                });
                                this.overlay.setImage(this.currentFloor.mapUrl);
                            }
                            console.log("masuk");
                            console.log('posisi',this.position);
                            //console.log('respon',res);
                            if(this.position.statusName == "USER_NOT_IN_BUILDING"){
                                this.stopPositioning(loading);
                                console.log("User not in Building");
                                this.presentToast("You Are not in Building",'bottom',null);
                                return;
                            }
                            if(!this.position || !this.position.coordinate) return;
                            let positionLatLngOnly = this.mountPositionCoords(this.position);
                            console.log('posisi baru',positionLatLngOnly);
                            //update navigation
                            if(this.navigating) this.updateNavigation(this.position);
                            this.currentFloorTxt = this.position.floorIdentifier;
                            this.marker.setPosition(positionLatLngOnly);
                            this.showRoute();
                            this.hideLoading(loading);
                            this.detector.detectChanges();
                            //tilt camera
                            if(this.map.getCameraTilt() < 15){
                                this.map.animateCamera({
                                    target:positionLatLngOnly,
                                    zoom:20,
                                    tilt:30
                                });
                            }
                            this.checkDistancePois(this.position,this.pois);
                            // if(this.promo){
                            //     this.isPositionInsideEvent(this.position);
                            //     if(this.isInsideEvent && !this.notShowAgain){
                            //         this.notifyPromo();
                            //     }
                            // }
                            //console.log(this.isInsideEvent);
                        },(err:any) => {
                            const reason = err.match("reason=(.*),");
                            let errorMsg = reason ? reason[1] : err;
                            this.stopPositioning(loading);
                            console.log('Error when starting Positioning', err);
                            const message = "Error when starting positioning " + errorMsg;
                            this.presentToast(message,'bottom',null);
                        });
                    }else{
                        //console.log(isEnabled);
                        this.hideLoading(loading);
                        const activateLocation = this.alertCtrl.create({
                        title:"Enable Location",
                        message:"You Must Enable Location to use this Service",
                        buttons:[{
                            text:"Dissmiss",
                            handler:()=>{
                                this.txt = "You Must Enable Location";
                                return;
                            }
                        },{
                            text:"Enable",
                            handler:()=>{
                                return this.diagnostic.switchToLocationSettings();
                            }
                        }]
                    });
                        activateLocation.present();
                    }
                }).catch(err =>{
                    console.log("error location",err);
                });
            } else{
                const message = "You Must have location permission granted";
                this.presentToast(message, 'bottom', null);
            }
        }).catch(error =>{
            console.log(error);
            const message = `Error when requesting for location permissions. ${error}`
            this.presentToast(message, 'bottom', null);
        });
    }

    private isPositionInsideEvent(position){
        if(position.floorIdentifier !== this.promo.trigger.center.floorIdentifier){
            console.log("not on floor");
            return false;
        }else{
            this.checkDistance(position,this.promo);
        }
        
    }

    private mountLocationOptions() {
        let locationOptions = new Array();
        locationOptions.push(this.building);
        defaultOptionsMap['buildingIdentifier'] = this.building.buildingIdentifier,
        locationOptions.push(defaultOptionsMap);
        return locationOptions;
      }
    
    private mountPositionCoords(position) : ILatLng {
        return {
          lat: position.coordinate.latitude,
          lng: position.coordinate.longitude
        };
      }

    private createPositionMarker() {
        let defaultOptions: MarkerOptions = {
          position: { lat: 0, lng: 0 },
          title: 'Current position',
          icon:{
              size:{
                width:35,
                height:35
              },
              url:"https://cdn1.iconfinder.com/data/icons/human-person-user-profile-business-avatars/100/25-1User_2-5-512.png"
          }
        };
        this.createMarker(defaultOptions, this.map, true);
      }

    private updateNavigation(position) {
        // Sends a position to the location manager for calculate the navigation progress
        cordova.plugins.Situm.updateNavigationWithLocation([position], function(error) {
          console.log("error update navigation",error);
        }, function (error) {
          console.log("error update navigation 2",error);
        });
      }
    
    private stopPositioning(loading) {
        if (this.positioning == false) {
          console.log("Position listener is not enabled.");
          this.hideLoading(loading);
          return;
        }
        cordova.plugins.Situm.stopPositioning(() => {
          //if (this.marker) this.marker.remove();
          if (this.polyline) {
            this.polyline.remove();
            this.route = null;
          }
          this.positioning = false;
          this.detector.detectChanges();
          if(loading) this.hideLoading(loading);
          this.presentToast("Positioning Stopped", 'bottom', null);
        });
        this.hideLoading(loading);
      }

    private showRoute(){
        if(!this.map || (!this.pois || this.pois.length == 0)){
            const message = "Map must be Visible and Have POIs";
            //this.hideLoading(loading);
            this.presentToast(message,'bottom',null);
        }
        if (this.polyline) {
            this.polyline.remove();
            this.route = null;
          }
        if (this.route) {
            const message = 'The route is already painted on the map.';
            //if(loading) this.hideLoading(loading);
            this.presentToast(message, 'bottom', null);
            return;
          }

        console.log("Your Position is:" + this.position.bearing.degrees);
        let directionsOptionsMap = {
            accessible : this.accessible,
            startingAngle: this.position.bearing.degrees,
        };
        let destination;
        if(this.promo){
            destination = this.promo.trigger.center;
        }else{
            destination = this.selectedPoi.position;
        }
        cordova.plugins.Situm.requestDirections([this.building,this.position,destination,directionsOptionsMap], (route:any) => {
            console.log(route);
            this.route = route;
            this.drawRouteOnMap(route);
            this.detector.detectChanges()
        },(error:any) => {
            console.log("error Directions", error);
            const message = "Error when Drawing Route, ${error}";
            this.presentToast(message, 'bottom', null);
            this.stopPositioning(null);
            return;
        });
    }

    private drawRouteOnMap(route) {
        if(this.polyline){
            this.polyline.remove();
        }
        let polylineOptions: PolylineOptions = {
          color: ROUTE_COLOR,
          width: 5,
          points: [],
          zIndex:20,
        };
        route.points.forEach(point => {
        if(point.floorIdentifier != this.currentFloorTxt){
            return;
        }else{
            polylineOptions.points.push({
                lat: point.coordinate.latitude,
                lng: point.coordinate.longitude
            });
        }
        });
        this.map.addPolyline(polylineOptions).then((polyline : Polyline) => {
          this.polyline = polyline;
        });
      }

      private updateAccessible() {
        console.log(`Accessibility status: ${this.accessible}`);
        this.accessible = !this.accessible;
        this.detector.detectChanges();
      }

      private requestNavigation(){
        var navigationOptions = {
            distanceToFloorChangeThreshold: 0,
            distanceToChangeIndicationThreshold: 0,
            distanceToGoalThreshold:0,
            outsideRouteThreshold:0,
            indicationsInterval:0,
            timeToFirstIndication:0,
            roundIndicationsStep:0,
            distanceToIgnoreFirstIndication:0,
            timeToIgnoreUnexpectedFloorChanges:0
        }
        navigationOptions.distanceToFloorChangeThreshold = 3; // number in meters
        navigationOptions.distanceToChangeIndicationThreshold = 2; // number in meters
        navigationOptions.distanceToGoalThreshold = 3; // number in meters
        navigationOptions.outsideRouteThreshold = 5; // number in meters
        navigationOptions.indicationsInterval = 2000; // number in millis
        navigationOptions.timeToFirstIndication = 2000; // number in millis
        navigationOptions.roundIndicationsStep = 1; // number in meters
        navigationOptions.distanceToIgnoreFirstIndication = 3; // number in meters
        navigationOptions.timeToIgnoreUnexpectedFloorChanges = 3000; // number in milliseconds

          if(this.navigating){
              const message = "Navigation is already activated";
              this.presentToast(message,'bottom',null);
              return;
          }
          //Add listener to navigation update when method updateNavigationWithLocation is called
          cordova.plugins.Situm.requestNavigationUpdates([navigationOptions],(res:any) => {
              console.log(res);
          },error => {
              console.log(error);
          });
          this.navigating = true;
          const message = "Added listener to receive navigation updates";
          this.presentToast(message,'bottom',null);
      }

      private removeNavigation() {
        if (!this.navigating) {
          const message = 'Navigation is already deactivated';
          this.presentToast(message, 'bottom', null);
          return;
        }
        // Removes the listener from navigation updates
        cordova.plugins.Situm.removeNavigationUpdates();
        this.navigating = false;
        const msg = 'Removed the listener from navigation updates';
        this.presentToast(msg, 'bottom', null);
      }

    private getBounds(building) {
        if (!building) return;
        return [
            { lat: building.bounds.southWest.latitude, lng: building.bounds.southWest.longitude },
            { lat: building.bounds.northEast.latitude, lng: building.bounds.northEast.longitude }
        ];
    }

    private hideLoading(loading) {
        if (typeof loading != undefined || typeof loading != null ) {
          loading.dismissAll();
          loading = null;
        }
        if(typeof loading == null){
            console.log('null loading');
        }
      }
    
      private createLoading(msg) {
        return this.loadCtrl.create({
          content: msg
        });
      }

      private handleError(error, loading) {
        this.hideLoading(loading);
      }

      presentToast(text, position, toastClass) {
        const toast = this.toastCtrl.create({
          message: text,
          duration: 3000,
          position: position,
          cssClass: toastClass ? toastClass : ''
        });
        toast.present();
      }

      private createMarker(options : MarkerOptions, map, currentPosition) {
        map.addMarker(options).then((marker : Marker) => {
          if (currentPosition) {
            this.marker = marker;
            this.marker.showInfoWindow();
          }
        });
      }

      checkDistance(position,promo){
        var from:ILatLng = {
            lat:position.coordinate.latitude,
            lng:position.coordinate.longitude
        }
        var to:ILatLng = {
            lat:promo.trigger.center.coordinate.latitude,
            lng:promo.trigger.center.coordinate.longitude
        }
        //console.log(from);
        //console.log(to);
        this.isInsideEvent = Spherical.computeDistanceBetween(from,to) <= promo.trigger.radius; 
        //console.log(this.isInsideEvent);
    }
    notifyPromo(){
        const eventNotif = this.alertCtrl.create({
            title:this.promo.name,
            message:this.promo.infoHtml,
            buttons:[{
                text:"Close",
                handler:()=>{
                    //this.notShowAgain = true;
                    return;
                }
            }]
        });
        eventNotif.present().then(()=>{
            this.notShowAgain = true;
        });
    }
    notifyPoi(poi){
        const poiNotif = this.alertCtrl.create({
            title:poi.poiName,
            message:poi.infoHtml,
            buttons:[{
                text:"Close",
                handler:()=>{
                    //this.notShowAgain = true;
                    return;
                }
            }]
        });
        if(!poi.notShowAgain){
            console.log("masuk notif");
            poiNotif.present().then(()=>{
                poi.notShowAgain = true;
            });
        }
    }
    checkDistancePois(position,pois){
        var from:ILatLng = {
            lat:position.coordinate.latitude,
            lng:position.coordinate.longitude
        }
        pois.forEach(poi =>{
            var to:ILatLng = {
                lat:poi.coordinate.latitude,
                lng:poi.coordinate.longitude
            }
            let testDistance = Spherical.computeDistanceBetween(from,to);
            //console.log(poi.poiName,testDistance);
            if(testDistance < 5){
                //this.notifyPoi(poi);
                this.nearestPoi = poi;
                console.log(poi.poiName,poi.notShowAgain);
            }
        });
    }
    test(){
        // console.log(this.building);
        // console.log(this.currentFloor);
        // console.log(this.position);
        var txt = this.building.name + ', Level ' + this.currentFloor.level + ', Latitude: ' + this.position.coordinate.latitude + ', Longitude: ' + this.position.coordinate.longitude + ', Near ' + this.nearestPoi.poiName;
        //var txt = "senarapp://list";
        console.log(txt);
        var shareOptions = {
            message:txt,
            subject:"Share Location",
            files:null,
            //url:"https://www.google.com/maps/@" + this.position.coordinate.latitude + ',' + this.position.coordinate.longitude,
            url:"www.somelink.com",
            chooserTitle:"Share Your Location"
        }
        this.socialSharing.shareWithOptions(shareOptions).then(() => {
            return;
        }).catch(error => {
            console.log(error);
        })
    }

    tambahBottomPx(index){
        //console.log(button.style.bottom);
        return (index + 4) * 30 + (index * 10);
    }

    gantiFloor(event,floor){
        //this.currentFloorTxt = floor.floorIdentifier;
        this.overlay.setImage(floor.mapUrl);
        let pois = [];
        this.pois.forEach(poi => {
            poi.findCategoryAgain = true;
            if(poi.floorIdentifier == floor.floorIdentifier){
                pois.push(poi);
            }
        })
        console.log(this.pois);
        console.log(pois);
        this.markerPoi.forEach(marker => {
            marker.remove();
        });
        this.drawPOIsOnMap(pois);
        this.detector.detectChanges();
    }
}