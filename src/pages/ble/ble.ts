import { Component } from '@angular/core';
import { NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { IBeacon } from '@ionic-native/ibeacon';
import { Device } from '@ionic-native/device';
import { _appIdRandomProviderFactory } from '@angular/core/src/application_tokens';

import * as _ from 'underscore';

/**
 * Generated class for the BlePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-ble',
  templateUrl: 'ble.html',
})
export class BlePage {

  beacons:any[] = [];
  incomingBeacon:any;
  txt:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private ibeacon:IBeacon, public platform:Platform,
    public device:Device, public alertCtrl:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BlePage');
  }

  // ionViewDidEnter(){
  //   this.platform.ready().then(() => {
  //       this.onBluetooth();
  //       this.start();
  //   })
  // }

  start(){
    this.ibeacon.requestAlwaysAuthorization();

      let delegate = this.ibeacon.Delegate();

      // Subscribe to some of the delegate's event handlers
      delegate.didRangeBeaconsInRegion()
      .subscribe(
        data => {
          let tempStore:any[] = [];
          console.log('didRangeBeaconsInRegion: ', data);
          if(this.beacons.length == 0) {
            this.beacons = data.beacons;
            console.log(this.beacons);
          }
          this.incomingBeacon = data.beacons;
          this.incomingBeacon.forEach(beacon => {
            Object.defineProperties(beacon,{
              seen:{
                  value:false,
                  writable:true
              },
              duplicate:{
                  value:false,
                  writable:true
              },
              notSeenAgain:{
                value:false,
                writable:true
              }
          });
            //console.log("assign beacon",beacon);
          });
          this.beacons.forEach(value => {
            this.incomingBeacon.forEach(beacon => {
              //console.log("ID yang di compare",value.major);
              if(value.major === beacon.major){
                  beacon.duplicate = true;
                  //console.log("duplikat nilai sama",beacon);
              }
              if(!beacon.duplicate){
                if(!beacon.seen){
                  tempStore.push(beacon);
                  //console.log("beacon yang di push",beacon);
                }
                beacon.seen = true;
              }
            });
            if(value.notSeenAgain == false){
              this.alertBeacon(value);
            }  
        });
        tempStore.forEach(value => {
          if(!value.duplicate){
            this.beacons.push(value);
          }
        })
      },
        error => console.error(error)
      );
      delegate.didStartMonitoringForRegion()
      .subscribe(
        data => console.log('didStartMonitoringForRegion: ', data),
        error => console.error()
      );
      delegate.didEnterRegion()
      .subscribe(
        data => {
          console.log('didEnterRegion: ', data);
        }
      );

      let beaconRegion = this.ibeacon.BeaconRegion('testBeacon','00000000-0000-0000-0000-000000001111');
      // '00000000-0000-0000-0000-000000001111'

      this.ibeacon.startMonitoringForRegion(beaconRegion)
      .then(()=>{
          this.ibeacon.startRangingBeaconsInRegion(beaconRegion).then(() => {
            console.log("started ranging beacons in region");
            this.txt = "Scanning now...";
          }).catch(error => {
            console.error("error ranging",error);
          });
        console.log("start monitoring for region");
      }).catch(error => {
        console.error(error);
      })
  }

  stop(){
    let beaconRegion = this.ibeacon.BeaconRegion('testBeacon','00000000-0000-0000-0000-000000001111');

      this.ibeacon.stopMonitoringForRegion(beaconRegion)
      .then(
        () => {
            this.ibeacon.stopRangingBeaconsInRegion(beaconRegion).then(()=>{
              console.log("stopped ranging beacons in region");
              this.txt = "Scanning Stopped!";
            }).catch(error => {
              console.error("error stopped rangingx",error);
            });
        console.log('Native layer received the request to stop monitoring');})
        .catch(
        error => console.error('Native layer failed to stop monitoring: ', error)
      );
  }
  cek(){
    console.log(this.beacons);
  }
  alertBeacon(beacon){
    const bleAlert = this.alertCtrl.create({
      title:beacon.major,
      message:beacon.uuid,
      buttons:[{
        text:"Dismiss",
        handler:() => {
          //beacon.notSeenAgain = true;
          return;
        }
      }]
    });
    bleAlert.present();
    beacon.notSeenAgain = true;
  }

  onBluetooth(){
    this.ibeacon.enableBluetooth();
  }
  offBluetooth(){
    this.ibeacon.disableBluetooth();
  }
}



