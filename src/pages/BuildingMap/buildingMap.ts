import { Component, ChangeDetectorRef } from '@angular/core';
import { 
    NavController,
    NavParams,
    LoadingController, 
    ToastController, 
    Platform,
    AlertController
} from 'ionic-angular';
import { 
    GoogleMaps, 
    GoogleMap, 
    GoogleMapsEvent, 
    GoogleMapOptions, 
    GoogleMapsAnimation,
    LatLng, 
    ILatLng, 
    GroundOverlayOptions, 
    GroundOverlay, 
    MarkerOptions, 
    MarkerIcon, 
    Marker, 
    PolylineOptions, 
    HtmlInfoWindow, 
    Polyline,
    LocationService,
    MyLocation,
    Spherical
} from '@ionic-native/google-maps';
import { 
    DeviceOrientation, 
    DeviceOrientationCompassHeading ,
    DeviceOrientationCompassOptions
} from '@ionic-native/device-orientation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { PermissionsService } from '../../services/permissions';

declare var cordova: any;

const defaultOptionsMap = {
    useDeadReckoning: false,
    interval: 100,
    indoorProvider: 'INPHONE',
    useBle: true,
    useWifi: true, 
    useGps: true,
    motionMode: 'BY_FOOT',
    useForegroundService: false,
    outdoorLocationOptions: {
        continuousMode: false,
        userDefinedThreshold: false,
        burstInterval: 1,
        averageSnrThreshold: 25.0
    },
    beaconFilters: [],
    smallestDisplacement: 1.0,
    realtimeUpdateInterval: 100,
};

const NavigationRequest ={
    distanceToGoalThreshold: 1,
    outsideRouteThreshold: 1,
    distanceToFloorChangeThreshold: 1,
    distanceToChangeIndicationThreshold: 1,
    indicationsInterval: 100,
    timeToFirstIndication: 1000,
    roundIndicationsStep: 10,
    timeToIgnoreUnexpectedFloorChanges: 3000
}

@Component({
  selector: 'building-map',
  templateUrl: 'buildingMap.html'
})

export class BuildingMap{
    txt:string="";
    building: any;  
    floorList: any[] = [];
    currentFloor: any;
    poisList: any[] = [];
    poisCategoryList: any[] = [];
    markers: { [key: number]: Marker } = {};
    lastOpenMarkerIndex: number;
    magneticHeading: number;
    currentLocation: any;
    routeIsShow: boolean = false;
    poi: any;
    isInsideEvent:boolean;
    notShowAgain:boolean = false;
    promo:any;
    routeEventIsShow: boolean = false;
    selectedPois: any = null;
    poiPanduan:any;

    overlay:GroundOverlay;
    currentFloorTxt:any;

    map: GoogleMap;
    marker: Marker;
    markerPoi:any[] = [];
    positioning: boolean = false;
    position: any = {
        statusName: '',
        floorIdentifier: '',
        x: -1,
        y: -1,
        accuracy: -1,
        bearing: '',

    }
    accessible: boolean = false;
    navigating: boolean = false;
    route: any;
    polyline: Polyline;
    subscription: any;
    loading: any;
    //overlay: GroundOverlay;
    tilt: number = 0;

    constructor(
      public NavController: NavController, 
      public NavParams: NavParams, 
      public LoadingController: LoadingController, 
      public ChangeDetectorRef: ChangeDetectorRef,
      public ToastController: ToastController,
      public Platform: Platform,
      public GoogleMaps: GoogleMaps,
      public AlertController: AlertController,
      public Spherical: Spherical,
      private DeviceOrientation: DeviceOrientation,
      private PermissionsService: PermissionsService,
      private Diagnostic: Diagnostic,
    ){
        this.building = NavParams.get('building');
        this.promo = NavParams.get('promo');
        this.poiPanduan = NavParams.get('poi');
    }

    ionViewDidEnter(){
        this.Platform.ready().then(() => {
            this.fetchFloorsFromBuilding(this.building);
            this.getHeading();
        }).catch(error =>{
            console.log('error',error);
        });
    }

    getHeading(){
        this.DeviceOrientation.getCurrentHeading().then(
            (data: DeviceOrientationCompassHeading) => this.magneticHeading = data.magneticHeading,
            (error: any) => console.log(error)
        );
    }

    ionViewWillLeave(){
        this.stopPositioning();
        this.stopNavigationUpdates();
        this.subscription.unsubscribe();
    }

    fetchFloorsFromBuilding(building){
        this.loading = null;
        if(this.floorList.length == 0){
            this.loading = this.createLoading("Loading building map...");
            this.loading.present();
        };
        cordova.plugins.Situm.fetchFloorsFromBuilding(building, (result: any) => {
            this.floorList = result;
            this.currentFloor = result[0];
            this.mountMap();
            this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
                this.mountOverlay();
            }).catch((err: any) =>  this.handleError(err));
            this.loading.dismiss();
        }, (error) => {
            this.loading.dismiss();
            console.log('error', error);
            this.presentToast(error, 'bottom', null);
        })
    }

    private mountMap() {
        let element = this.getElementById('map');
        let options: GoogleMapOptions = {
            camera: {
                target: this.getCenter(this.building),
                zoom: 18
            },
            controls: {
                'compass': true,
                'myLocationButton': false,
                'myLocation': false,   // (blue dot)
                'indoorPicker': false,
                'zoom': false,          // android only
                'mapToolbar': false,
            },
            styles: [
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        { "visibility": "off" }
                    ]
                }
            ],
        };
        this.map = GoogleMaps.create(element, options);
    }

    private mountOverlay() {
        let bounds = this.getBounds(this.building);
        let bearing = (this.building.rotation * 180 / Math.PI);
        let groundOptions: GroundOverlayOptions = {
            url: this.currentFloor.mapUrl,
            bounds: bounds,
            bearing: bearing,
            anchor: [0.5, 0.5]
        }
        this.map.addGroundOverlay(groundOptions).then((overlay:GroundOverlay) => {
            this.fetchForPOIs(this.building);
            this.overlay = overlay;
        }).catch((err: any) => this.handleError(err));
    }

    private fetchForPOIs(building) {
        cordova.plugins.Situm.fetchIndoorPOIsFromBuilding(building, (res: any) => {
            this.poisList = res;
            if (this.poisList.length == 0) {
                this.loading.dismiss();
                const message = 'This building has no POIs';
                this.presentToast(message, 'bottom', null);
                return;
            }
            this.ChangeDetectorRef.detectChanges();
            this.fetchForPOICategories(building);
        });
    }

    private fetchForPOICategories(building) {
        cordova.plugins.Situm.fetchPoiCategories((res: any) => {
            this.poisCategoryList = res;
            this.drawPOIsOnMap(this.poisList);
            if(!this.promo && !this.poi){
                this.loading.dismiss();
            }else if(this.promo){
                this.drawMarker();
            }else{
                this.startPositioning();
            }
        });
    }

    private drawMarker(){
        let markerPosition: ILatLng = {
            lat: this.promo.trigger.center.coordinate.latitude,
            lng: this.promo.trigger.center.coordinate.longitude
        };
        let icon: MarkerIcon = {
            size: {
                height: 35,
                width: 35
            }
        }
        let markerOptions: MarkerOptions = {
            icon: icon,
            position: markerPosition,
            title: this.promo.name,
        };
        this.createEventMarker(markerOptions, this.map, false, this.promo);
        this.startPositioning();
    }

    private createEventMarker(options : MarkerOptions, map, currentPosition, event: any) {
        map.addMarker(options).then((marker : Marker) => {
            if (currentPosition) {
                this.marker = marker;
                this.marker.showInfoWindow();
            }
        });
    }

    private showRouteToEvent(){
        console.log('this.position', this.position)
        this.routeEventIsShow = true;
        /*if (!this.map || !this.positioning) {
            const message = 'The map with the POIs must be visible and the positioning must be started in order to determine the route';
            this.presentToast(message, 'bottom', null);
            return;
        }*/

        if (this.route) {
            this.polyline.remove();
            this.route = null;
        }
        
        let directionsOptionsMap = {
            accessibleRoute: this.accessible, 
            startingAngle: this.position.bearing.degrees,
        };
        
        cordova.plugins.Situm.requestDirections([this.building, this.position, this.promo.trigger.center, directionsOptionsMap], (route: any) => {
            this.route = route;
            this.drawRouteOnMap(this.route);
            this.ChangeDetectorRef.detectChanges();
        }, (err: any) => {
            console.error(err);
            //const message = `Error when drawing the route. ${err}`;
            //this.presentToast(message, 'bottom', null);
            return;
        });
    }

    private drawPOIsOnMap(pois) {
        pois.forEach((poi, index) => {
            poi.is_selected = false;
            if(!poi.findCategoryAgain){
                Object.assign(poi,{findCategoryAgain:false});
            }
            if(!poi.findCategoryAgain){
                poi.category = this.findPOICategory(poi);
            }
            let markerPosition: ILatLng = {
                lat: poi.coordinate.latitude,
                lng: poi.coordinate.longitude
            }
            let icon: MarkerIcon = {
                size: {
                    height: 32,
                    width: 32
                }
            }
            if (poi.category) icon.url = 'https://dashboard.situm.es' + poi.category.icon_unselected;
            let markerOptions: MarkerOptions = {
                icon: icon,
                position: markerPosition,
                title: `${poi.poiName}`,
            };
            this.createMarker(markerOptions, this.map, false, poi, index);
        });
    }

    removePath(){
        this.polyline.remove();
        this.route = null;
    }
    private createMarker(options : MarkerOptions, map, currentPosition, poi: any, index: number) {
        this.poi = poi;
        map.addMarker(options).then((marker : Marker) => {
            this.markerPoi.push(marker);
            if (currentPosition) {
                this.marker = marker;
                this.marker.showInfoWindow();
            }
            marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe((params) => {
                this.selectedPois = poi;
                let iconSelected: MarkerIcon = {
                    url: 'https://dashboard.situm.es' + poi.category.icon_selected,
                    size: {
                        width: 34,
                        height: 34
                    }
                };
                this.showRoute(poi);
                // marker.setIcon(iconSelected);
                /*if (this.lastOpenMarkerIndex !== undefined) {
                    this.changeLastMarkerToInactive();
                }*/
                // this.lastOpenMarkerIndex = index;
            });
            // this.markers[index] = marker;
        });
    }

    private startPositioning(){
        this.PermissionsService.checkLocationPermissions().then(permission => {
            if (permission) {
                this.Diagnostic.isLocationEnabled().then((isEnabled)=>{
                    if(isEnabled){
                        if (this.positioning == true) {
                            const message = 'Position listener is already enabled.';
                            this.presentToast(message, 'bottom', null);
                            return;
                        }
                        if(!this.map){
                            const message = "Map is not Available";
                            this.presentToast(message,'bottom',null);
                            return;
                        }
                        this.loading = this.createLoading("Getting your position...");
                        this.loading.present();
                        this.createPositionMarker();
                        const locationOptions = this.mountLocationOptions();
                        cordova.plugins.Situm.startPositioning(locationOptions, (res: any) => {
                            console.log('positionings', res)
                            this.positioning = true;
                            this.position = res;
                            // if(this.currentFloor.floorIdentifier !== res.floorIdentifier){
                            //     this.floorList.forEach(floor => {
                            //         if(floor.floorIdentifier == res.floorIdentifier){
                            //             this.currentFloor = floor;
                            //         }
                            //     })
                            //     this.overlay.setImage(this.currentFloor.mapUrl);
                            // }
                            if(res.statusOrdinal == 6){
                                this.stopPositioning();
                                this.presentToast("You are Currently not in the building", "bottom", null);
                            }
                            this.currentFloorTxt = this.position.floorIdentifier;
                            if(this.currentFloor.floorIdentifier !== res.floorIdentifier){
                                this.floorList.forEach(floor => {
                                    if(floor.floorIdentifier == res.floorIdentifier){
                                        this.currentFloor = floor;
                                        if(!this.currentFloorTxt) this.currentFloorTxt = this.currentFloor.floorIdentifier;
                                    }
                                });
                                this.gantiFloor(this.currentFloor);
                            }
                            if (!this.position || !this.position.coordinate) return;
                            let position = this.mountPositionCoords(this.position);
                            // Update the navigation
                             if (this.navigating){
                                this.updateNavigation(this.position);
                                if(this.selectedPois != null) {
                                    this.checkDistancePois(this.position, this.selectedPois);
                                }
                                //this.showRoute(this.selectedPois);
                                if(this.promo != undefined){
                                    this.showRouteToEvent();
                                }
                            }
                            this.marker.setPosition(position);
                            this.map.animateCamera({
                                target: {lat: position.lat, lng: position.lng},
                                zoom: 35,
                                tilt: this.tilt,
                                duration: 500
                            }).then(() => {
                            //alert("Camera target has been changed");
                            });
                            this.loading.dismiss();
                            if(this.promo){
                                this.isPositionInsideEvent(this.position);
                            }
                            if(this.poiPanduan){
                                if(this.polyline) this.polyline.remove();
                                this.showRoute(this.poiPanduan);
                            }
                            this.ChangeDetectorRef.detectChanges();
                        }, (err: any) => {
                            const reason = err.match("reason=(.*),");
                            let errorMessage =  reason ? reason[1] : err;
                            this.stopPositioning();
                            console.log('Error when starting positioning.', err);
                            const message = `Error when starting positioning. ${errorMessage}`;
                            this.presentToast(message, 'bottom', null);
                            this.loading.dismiss();
                        });
                    }else{
                        this.loading.dismiss();
                        const activateLocation = this.AlertController.create({
                            title:"Enable Location",
                            message:"You Must Enable Location to use this Service",
                            buttons:[{
                                text:"Dissmiss",
                                handler:()=>{
                                    this.txt = "You Must Enable Location";
                                    return;
                                }
                            },{
                                text:"Enable",
                                handler:()=>{
                                    return this.Diagnostic.switchToLocationSettings();
                                }
                            }]
                        });
                        activateLocation.present();
                    }
                }).catch(error=>{
                    console.log('error',error);
                })
            } else {
                const message = `You must have the location permission granted for positioning.`
                this.presentToast(message, 'bottom', null);
            }
        }).catch(error => {
            console.log(error);
            const message = `Error when requesting for location permissions. ${error}`
            this.presentToast(message, 'bottom', null);
        });
    }

    private stopPositioning() {
        if (this.positioning == false) {
            console.log("Position listener is not enabled.");
            return;
        }
        cordova.plugins.Situm.stopPositioning(() => {
            if (this.marker) this.marker.remove();
            if (this.polyline) {
                this.polyline.remove();
                this.route = null;
            }
            this.positioning = false;
            this.ChangeDetectorRef.detectChanges();
            this.loading.dismiss();
        });
    }

    private createPositionMarker() {
        let defaultOptions: MarkerOptions = {
            icon: {
                url: "https://image.winudf.com/v2/image/Ymx1ZWRvdC5tZS5nbG9iYWxfaWNvbl8xNTE3NjMzMzA1XzA4MA/icon.png?w=170&fakeurl=1&type=.png",
                size: {
                    width: 24,
                    height: 24
                }
            },
            position: { lat: 0, lng: 0 },
            title: '',
            flat: true,
        };
        this.map.addMarker(defaultOptions).then((marker : Marker) => {
            this.marker = marker;
            this.marker.showInfoWindow();
        });
    }

    private updateMapRotation(position){
        let options : DeviceOrientationCompassOptions = {
            frequency: 30
        }
        this.subscription = this.DeviceOrientation.watchHeading(options).subscribe(
            (data: DeviceOrientationCompassHeading) => {
                this.map.setCameraBearing(data.magneticHeading);
            }
        );
    }

    private startNavigationUpdates() {
        this.tilt = 45;
        this.DeviceOrientation.getCurrentHeading().then(
            (data: DeviceOrientationCompassHeading) => {
                this.map.animateCamera({
                    target: {lat: this.position.coordinate.latitude, lng: this.position.coordinate.longitude},
                    zoom: 35,
                    tilt: this.tilt,
                    bearing: data.magneticHeading,
                    duration: 1000
                }).then(() => {
                //alert("Camera target has been changed");
                });
            },
            (error: any) => console.log(error)
        );
        if (this.navigating) {
            const message = 'Navigation is already activated';
            this.presentToast(message, 'bottom', null);
            return;
        }
        // Adds a listener to receive navigation updates when the 
        // updateNavigationWithLocation method is called
        cordova.plugins.Situm.requestNavigationUpdates(NavigationRequest, 
            (res: any) => {console.log(res)}, 
            (err: any) => {console.log(err)}
        );
        this.navigating = true;
        this.updateMapRotation(this.position);
        const msg = 'Added a listener to receive navigation updates';
        this.presentToast(msg, 'bottom', null);
    }

    private stopNavigationUpdates() {
        if (!this.navigating) {
            const message = 'Navigation is already deactivated';
            this.presentToast(message, 'bottom', null);
            return;
        }
        // Removes the listener from navigation updates
        cordova.plugins.Situm.removeNavigationUpdates();
        this.tilt = 0;
        this.navigating = false;
        this.polyline.remove();
        this.subscription.unsubscribe();
        this.route = null;
        const msg = 'Removed the listener from navigation updates';
        this.presentToast(msg, 'bottom', null);
    }

    private updateNavigation(position) {
        cordova.plugins.Situm.updateNavigationWithLocation([position], function(error) {
            console.log(error);
        }, function (error) {
            console.log(error);
        });
    }

    private mountLocationOptions() {
        let locationOptions = new Array();
        locationOptions.push(this.building);
        defaultOptionsMap['buildingIdentifier'] = this.building.buildingIdentifier,
        locationOptions.push(defaultOptionsMap);
        return locationOptions;
    }

    private mountPositionCoords(position) : ILatLng {
        return {
            lat: position.coordinate.latitude,
            lng: position.coordinate.longitude
        };
    }

    private findPOICategory(poi) {
        return this.poisCategoryList.find((poiCategory: any) => {
            return poiCategory.poiCategoryCode == poi.category;
        });
    }

    private getCenter(building) : LatLng {
        return new LatLng(building.center.latitude, building.center.longitude);
    }

    /*private changeLastMarkerToInactive() {
        let iconUnselected: MarkerIcon = {
            url: 'https://dashboard.situm.es' + this.poisList[this.lastOpenMarkerIndex].category.icon_unselected,
            size: {
                width: 32,
                height: 32
            }
        };
        this.markers[this.lastOpenMarkerIndex].setIcon(iconUnselected);
        console.log('polyline', this.polyline)
        this.polyline.remove();
        this.route = null;
    }*/

    private getBounds(building) {
        if (!building) return;
        return [
            { lat: building.bounds.southWest.latitude, lng: building.bounds.southWest.longitude },
            { lat: building.bounds.northEast.latitude, lng: building.bounds.northEast.longitude }
        ];
    }

    private getElementById(id) : HTMLElement {
        return document.getElementById(id);
    }

    private handleError(error) {
        this.loading.dismiss();
    }

    presentToast(text, position, toastClass) {
        const toast = this.ToastController.create({
            message: text,
            duration: 3000,
            position: position,
            cssClass: toastClass ? toastClass : ''
        });
        toast.present();
    }

    createLoading(msg) {
        return this.LoadingController.create({
            content: msg
        });
    }

    private showRoute(poi) {
        this.routeIsShow = true;
        if (!this.map || (!this.poisList|| this.poisList.length == 0) || !this.positioning) {
            const message = 'The map with the POIs must be visible and the positioning must be started in order to determine the route';
            this.presentToast(message, 'bottom', null);
            return;
        }

        if (this.route) {
            // const message = 'The route is already painted on the map.';
            // this.presentToast(message, 'bottom', null);
            // return;
            this.polyline.remove();
            this.route = null;
        }
        
        let directionsOptionsMap = {
            accessibleRoute: this.accessible, 
            startingAngle: this.position.bearing.degrees,
        };

        // Calculates a route between two points
        // In this case, determining route between the current position and the second POI
        cordova.plugins.Situm.requestDirections([this.building, this.position, poi, directionsOptionsMap], (route: any) => {
            this.route = route;
            this.drawRouteOnMap(this.route);
            this.ChangeDetectorRef.detectChanges();
        }, (err: any) => {
            console.error(err);
            //const message = `Error when drawing the route. ${err}`;
            //this.presentToast(message, 'bottom', null);
            return;
        });
    }

    private drawRouteOnMap(route) {
        let polylineOptions: PolylineOptions = {
            color: "#e80180",
            width: 3,
            points: [],
            zIndex: 9999,
        };
        route.points.forEach(point => {
            polylineOptions.points.push({
                lat: point.coordinate.latitude,
                lng: point.coordinate.longitude
            });
        });
        this.map.addPolyline(polylineOptions).then((polyline : Polyline) => {
            this.polyline = polyline;
        });
    }

     private isPositionInsideEvent(position){
        if(position.floorIdentifier !== this.promo.trigger.center.floorIdentifier){
            console.log("not on floor");
            return false;
        }else{
            this.checkDistance(position,this.promo);
        }
    }

    checkDistance(position,promo){
        var from:ILatLng = {
            lat:position.coordinate.latitude,
            lng:position.coordinate.longitude
        }
        var to:ILatLng = {
            lat:promo.trigger.center.coordinate.latitude,
            lng:promo.trigger.center.coordinate.longitude
        }
        //console.log(from);
        //console.log(to);
        this.isInsideEvent = Spherical.computeDistanceBetween(from,to) <= promo.trigger.radius; 
        //console.log(this.isInsideEvent);
    }

    notifyPromo(){
        const eventNotif = this.AlertController.create({
            title:this.promo.name,
            message:this.promo.infoHtml,
            buttons:[{
                text:"Close",
                handler:()=>{
                    //this.notShowAgain = true;
                    return;
                }
            }]
        });
        eventNotif.present().then(()=>{
            this.notShowAgain = true;
        });
        this.loading.dismiss();
    }

    notifyPoi(poi){
        const poiNotif = this.AlertController.create({
            title: poi.poiName,
            message: "You have arrive in location",
            buttons:[{
                text:"Close",
                handler:()=>{
                    //this.notShowAgain = true;
                    return;
                }
            }]
        });
        if(!poi.notShowAgain){
            console.log("masuk notif");
            poiNotif.present().then(()=>{
                poi.notShowAgain = true;
            });
        }
        this.polyline.remove();
        this.route = null;
        this.loading.dismiss();
    }

    checkDistancePois(position, pois){
        var from:ILatLng = {
            lat:position.coordinate.latitude,
            lng:position.coordinate.longitude
        }

        var to:ILatLng = {
            lat: pois.coordinate.latitude,
            lng: pois.coordinate.longitude
        }

        let testDistance = Spherical.computeDistanceBetween(from,to);
        if(testDistance < 2){
            this.notifyPoi(pois);
        }

        /*pois.forEach(poi =>{
            var to:ILatLng = {
                lat:poi.coordinate.latitude,
                lng:poi.coordinate.longitude
            }
            let testDistance = Spherical.computeDistanceBetween(from,to);
            if(testDistance < 2){
                this.notifyPoi(poi);
            }
        });*/
    }

    tambahBottomPx(index){
        //console.log(button.style.bottom);
        return (index + 4) * 30 + (index * 10);
    }

    gantiFloor(floor){
        this.currentFloorTxt = floor.floorIdentifier;
        this.overlay.setImage(floor.mapUrl);
        let pois = [];
        this.poisList.forEach(poi => {
            poi.findCategoryAgain = true;
            if(poi.floorIdentifier == this.currentFloorTxt){
                pois.push(poi);
            }
        })
        // console.log(this.pois);
        // console.log(pois);
        this.markerPoi.forEach(marker => {
            marker.remove();
        });
        this.drawPOIsOnMap(pois);
        this.ChangeDetectorRef.detectChanges();
    }
}