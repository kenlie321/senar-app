import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, Platform, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { IBeacon } from '@ionic-native/ibeacon';
import { Diagnostic } from '@ionic-native/diagnostic';

import { PromoList } from '../PromoList/promoList';
import { CategoryList } from '../CategoryList/categories';

import { ThrowStmt } from '@angular/compiler';
import { BuildingDashboard } from '../BuildingDashboard/dashboard';
import { BuildingListPage } from '../BuildingList/buildingList';

declare var cordova:any;

@Component({
  selector: 'tabs-display',
  templateUrl: 'tabs.html'
})

export class TabsPage{
  building:any;

  isEvent:any;
  isMall:any;

  beacons:any[] = [];
  incomingBeacon:any;

  tab1Root:any;
  tab2Root:any;
  tab3Root:any;

    constructor(public navCtrl:NavController,public navParam:NavParams, public platform:Platform,
      public LoadingController:LoadingController, public ToastController:ToastController,
      public ChangeDetectorRef: ChangeDetectorRef, public alertCtrl:AlertController, public ibeacon:IBeacon,
      public diagnostic:Diagnostic)
      {
        this.tab1Root = PromoList;
        this.tab2Root = BuildingDashboard;
        this.tab3Root = CategoryList;

        this.building = this.navParam.get('building');
        
        this.isEvent = this.navParam.get('isEvent');
        this.isMall = this.navParam.get('isMall');
      }

    ionViewDidEnter(){
      this.platform.ready().then(() =>{
          //this.onBluetooth();
          //Start Searching for Bluetooth
          // this.start();

          // this.platform.pause.subscribe(()=>{
          //   this.stop();
          // });
          // this.platform.resume.subscribe(()=>{
          //   this.start();
          // });
      }).catch(error => {
        console.log(error);
      });
    }

    ionViewDidLeave(){
      this.stop();
    }

    test(){
      this.navCtrl.setRoot(BuildingListPage,{isEvent:this.isEvent,isMall:this.isMall})
    }

  //BLE CORNER

  start(){
    this.ibeacon.requestAlwaysAuthorization();

      let delegate = this.ibeacon.Delegate();

      // Subscribe to some of the delegate's event handlers
      delegate.didRangeBeaconsInRegion()
      .subscribe(
        data => {
          let tempStore:any[] = [];
          console.log('didRangeBeaconsInRegion: ', data);
          if(this.beacons.length == 0) {
            this.beacons = data.beacons;
            console.log(this.beacons);
          }
          this.incomingBeacon = data.beacons;
          this.incomingBeacon.forEach(beacon => {
              Object.defineProperties(beacon,{
                seen:{
                    value:false,
                    writable:true
                },
                duplicate:{
                    value:false,
                    writable:true
                },
                notSeenAgain:{
                  value:false,
                  writable:true
                }
            });
          });
          this.beacons.forEach(value => {
            this.incomingBeacon.forEach(beacon => {
              
              if(value.major === beacon.major){
                  beacon.duplicate = true;
              }
              if(!beacon.duplicate){
                if(!beacon.seen){
                  tempStore.push(beacon);
                }
                beacon.seen = true;
              }
            });
            if(value.notSeenAgain == false){
              if(value.major == "1" || value.major == "3"){
                this.alertBeacon(value);
              }
            }  
        });
        tempStore.forEach(value => {
          if(!value.duplicate){
            this.beacons.push(value);
          }
        })
      },
        error => console.error(error)
      );
      delegate.didStartMonitoringForRegion()
      .subscribe(
        data => console.log('didStartMonitoringForRegion: ', data),
        error => console.error()
      );
      delegate.didEnterRegion()
      .subscribe(
        data => {
          console.log('didEnterRegion: ', data);
        }
      );

      let beaconRegion = this.ibeacon.BeaconRegion('testBeacon','00000000-0000-0000-0000-000000001111');
      // '00000000-0000-0000-0000-000000001111'
      this.diagnostic.isBluetoothEnabled().then(isEnabled => {
        if(isEnabled){
          this.ibeacon.startMonitoringForRegion(beaconRegion)
          .then(()=>{
              this.ibeacon.startRangingBeaconsInRegion(beaconRegion).then(() => {
                console.log("started ranging beacons in region");
                //this.txt = "Scanning now...";
              }).catch(error => {
                console.error("error ranging",error);
              });
            console.log("start monitoring for region");
          }).catch(error => {
            console.error(error);
          });
        }else{
          const bleAlert = this.alertCtrl.create({
            title:"Enable Bluetooth?",
            message:"To fully experience Senar App, Please Enable Bluetooth to receive Notifications.",
            buttons:[{
              text:"Enable",
              handler:()=>{
                if(this.platform.is('android')){
                  this.diagnostic.setBluetoothState(true).then(() => {
                    setTimeout(() => {
                      this.start();
                    }, 2500);
                  })
                  return;
                }if(this.platform.is('ios')){
                  this.diagnostic.switchToBluetoothSettings();
                  this.start();
                  return;
                }
              }
            }]
          });
          bleAlert.present();
        } 
      }).catch(error => {
        console.log(error);
      })
      
  }

  stop(){
    let beaconRegion = this.ibeacon.BeaconRegion('testBeacon','00000000-0000-0000-0000-000000001111');

      this.ibeacon.stopMonitoringForRegion(beaconRegion)
      .then(
        () => {
            this.ibeacon.stopRangingBeaconsInRegion(beaconRegion).then(()=>{
              console.log("stopped ranging beacons in region");
              //this.txt = "Scanning Stopped!";
            }).catch(error => {
              console.error("error stopped rangingx",error);
            });
        console.log('Native layer received the request to stop monitoring');})
        .catch(
        error => console.error('Native layer failed to stop monitoring: ', error)
      );
  }
    cek(){
      console.log(this.beacons);
    }
    alertBeacon(beacon){
      let tempObject = {
        title:"",
        message:""
      }
      if(beacon.major == "3"){
        tempObject.title = "Buy 1 Get 1!";
        tempObject.message = "Buy Any Coffee Hot/Cold Get 1 Grande Frappuccino Free! Only At PPU Coffee!";
      }
      else if(beacon.major == "1"){
        tempObject.title = "50% OFF!!!";
        tempObject.message = "Get 50% Off A Dozen Donuts Today Only! At your Nearest PPU Donuts!";
      }
      // else{
      //   tempObject.title = "Senar App Demo!";
      //   tempObject.message = "Try Our Brand New App, Senar! Only At PPU Booth @TechInAsiaJCC!";
      // }
      const bleAlert = this.alertCtrl.create({
        title:tempObject.title,
        message:tempObject.message,
        buttons:[{
          text:"Dismiss",
          handler:() => {
            //beacon.notSeenAgain = true;
            return;
          }
        }]
      });
      bleAlert.present();
      beacon.notSeenAgain = true;
    }

    onBluetooth(){
      this.ibeacon.enableBluetooth();
    }
    offBluetooth(){
      this.ibeacon.disableBluetooth();
    }
    
}