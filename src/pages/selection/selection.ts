import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { BuildingListPage } from '../BuildingList/buildingList';

/**
 * Generated class for the SelectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-selection',
  templateUrl: 'selection.html',
})
export class SelectionPage {
  isEvent:boolean = false;
  isMall:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectionPage');
  }

  eventTapped(event){
    this.navCtrl.push(BuildingListPage, {isEvent:true});
  }

  mallTapped(event){
    this.navCtrl.push(BuildingListPage, {isMall:true});
  }

}
