import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { NavController, Slides, NavParams, LoadingController, ToastController, Platform } from 'ionic-angular';

import { PromoList } from '../PromoList/promoList';
import { CategoryList } from "../CategoryList/categories";
import { POIList } from '../POIList/poi';
import { PromoDetail } from '../PromoDetail/promoDetail';
import { BuildingMap } from '../BuildingMap/buildingMap';
import { MapDisplay } from '../mapDisplay/map';

declare var cordova: any;

@Component({
  selector: 'building-dashboard',
  templateUrl: 'dashboard.html'
})

export class BuildingDashboard{
    @ViewChild(Slides) Slides:Slides;
    building: any;  
    promos: any[] = [];
    categories: any[] = [];
    isEvent:any;
    isMall:any;
    isLoading:boolean = false;
    public slidesCurrentIndex: number = 0;

    constructor(
      public navCtrl:NavController, 
      public navParams:NavParams, 
      public loadCtrl: LoadingController, 
      public detector:ChangeDetectorRef,
      public toastCtrl:ToastController,
      public platform: Platform,
    ){
        this.building = navParams.get('building');
        this.isEvent = navParams.get('isEvent');
        this.isMall = navParams.get('isMall');

    }

    ionViewDidEnter(){
      this.platform.ready().then(() => {
        this.fetchEventsFromBuilding(this.building);
      }).catch(error =>{
        console.log('error',error);
      });
    }

    fetchEventsFromBuilding(building){
      let loading = null;
      if(this.promos.length == 0){
        loading = this.loadCtrl.create({content:'Loading Promos...'});
        this.isLoading = true;
        loading.present();
      }
      cordova.plugins.Situm.fetchEventsFromBuilding(building,(result: any) => {
        console.log('promo', result);
        if(result.length > 3){
          this.promos = result.slice(0,3);
        }else{
          this.promos = result;
        }
        if (this.promos.length == 0) {
          this.hideLoading(loading);
          this.fetchPoiCategories(loading);
          const message = 'This building has no Events';
          this.presentToast(message, 'bottom', null);
          return;
        }
        this.slidesCurrentIndex = 1;
        this.detector.detectChanges();
        this.fetchPoiCategories(loading);
      }, (error) => {
        const errorMsg = 'An error occurred when recovering the buildings.' 
        console.log(`${errorMsg}`, error);
        if(loading) loading.dismiss();
        this.presentToast(`${errorMsg} ${error}`, 'bottom', null);
      });
    };

    slideChanged() {
      if(this.Slides.length() > 1){
        if((this.Slides.getActiveIndex()) == this.Slides.length()){
          return;
        }else{
          this.slidesCurrentIndex = this.Slides.getActiveIndex() + 1;
        }
      }
  }

    fetchPoiCategories(loading){
      cordova.plugins.Situm.fetchPoiCategories((res:any)=>{
        if(this.isEvent){
          this.categories = res.filter((category) => {
            return category.poiCategoryCode.includes('evt');
          });
          if(this.categories.length > 5) this.categories = this.categories.slice(0,5);
        }else{
          this.categories = res.filter((category) => {
            return category.poiCategoryCode.includes('mall');
          });
          if(this.categories.length > 5) this.categories = this.categories.slice(0,5);
        }
        this.hideLoading(loading);
        console.log('response',res);
        console.log(this.categories);
      });
    }

    promoTapped(event,promo){
      this.navCtrl.push(PromoDetail,{promo:promo, building:this.building, isEvent:this.isEvent,isMall:this.isMall});
    }

    promoAllTapped(event){
      this.navCtrl.push(PromoList,{building:this.building, isEvent:this.isEvent,isMall:this.isMall});
    }

    categoryTapped(event, building, category){
      this.navCtrl.push(POIList, {building: building, category: category,isEvent:this.isEvent,isMall:this.isMall});
    }

    moreTapped(event, building){
      this.navCtrl.push(CategoryList, {building: building, isEvent:this.isEvent, isMall:this.isMall});
    }

    showBuildingMap(event, building){
      this.navCtrl.push(BuildingMap, {building: building});
    }

    mapButtonTapped(event){
      this.navCtrl.push(MapDisplay, {building:this.building});
    }

    cek(){
      console.log(this.building);
    }

    private hideLoading(loading) {
      if (typeof loading != undefined && typeof loading != null ) {
        loading.dismissAll();
        loading = null;
        this.isLoading = false;
      }
    }

    presentToast(text, position, toastClass) {
      const toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position: position,
        cssClass: toastClass ? toastClass : ''
      });
      toast.present();
    }
}