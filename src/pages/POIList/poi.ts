import { Component, ChangeDetectorRef } from '@angular/core';
import { 
	NavController, 
	NavParams,
	AlertController, 
    Platform, 
    ToastController, 
    LoadingController,
} from 'ionic-angular';
import { POIDetail } from '../POIDetail/poiDetail';
import { BuildingMap } from '../BuildingMap/buildingMap';

declare var cordova: any;

@Component({
  selector: 'poi-list',
  templateUrl: 'poi.html'
})

export class POIList{
	public building: any;
	public category: any;
	public floor: string = "";
	public poisList: any[] = [];
	public floorList: any[] = [];
	public isEvent:any;
	public isMall:any;

    constructor(
    	public NavController:NavController, 
    	public NavParams:NavParams,
    	public LoadingController: LoadingController,
    	public ChangeDetectorRef: ChangeDetectorRef,
    	public ToastController: ToastController
    ){
    	this.building = NavParams.get('building'),
		this.category = NavParams.get('category'),
		this.isEvent = NavParams.get('isEvent'),
		this.isMall = NavParams.get('isMall')
    }
    
    ionViewDidEnter(){
    	this.fetchIndoorPOIsFromBuilding(this.building);
    	this.fetchFloorsFromBuilding(this.building);
    	this.floor = "all";
    	console.log('building', this.building);
    	console.log('category', this.category);
    }

    fetchIndoorPOIsFromBuilding(building){
    	let loading = null;
        if(this.poisList.length == 0) {
            loading = this.LoadingController.create({content: 'Loading POIs...'});
            loading.present();
        }
    	cordova.plugins.Situm.fetchIndoorPOIsFromBuilding(building, (result) => {
    		console.log('indoor poi', result)
    		this.poisList = [];
    		for(var i = 0; i < result.length; i++){
    			if(result[i].category == this.category.poiCategoryCode){
    				this.poisList.push(result[i])
    			}
    		}
    		this.ChangeDetectorRef.detectChanges();
    		loading.dismiss();
    	}, (error) => {
    		const errorMessage = "An error occurred when recovering the POIs.";
            console.log(`${errorMessage}`, error);
            if(loading) loading.dismiss();
            this.presentToast(`${errorMessage} ${error}`, 'bottom', null);
    	})
    }

    fetchFloorsFromBuilding(building){
    	let loading = null;
        if(this.floorList.length == 0) {
            loading = this.LoadingController.create({content: 'Loading floors...'});
            loading.present();
        }
    	cordova.plugins.Situm.fetchFloorsFromBuilding(building, (result) => {
    		console.log('floor', result)
    		this.floorList = result;
    		this.ChangeDetectorRef.detectChanges();
    		loading.dismiss();
    	}, (error) => {
    		const errorMessage = "An error occurred when recovering the floors.";
            console.log(`${errorMessage}`, error);
            if(loading) loading.dismiss();
            this.presentToast(`${errorMessage} ${error}`, 'bottom', null);
    	})
    }

    presentToast(text, position, toastClass) {
        const toast = this.ToastController.create({
            message: text,
            duration: 3000,
            position: position,
            cssClass: toastClass ? toastClass : ''
        });
        toast.present();
    }

    poiTapped(event, building, poi){
      this.NavController.push(POIDetail, {building: building, poi: poi, isEvent:this.isEvent,isMall:this.isMall});
	}
	
	showBuildingMap(event){
		this.NavController.push(BuildingMap, {building: this.building});
	  }
}