import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { PermissionsService } from '../services/permissions';

import { MyApp } from './app.component';
import { BuildingListPage } from '../pages/BuildingList/buildingList';
import { ListPage } from '../pages/list/list';
import { BuildingDashboard } from '../pages/BuildingDashboard/dashboard';
import { PromoList } from '../pages/PromoList/promoList';
import { PromoDetail } from '../pages/PromoDetail/promoDetail';
import { CategoryList } from '../pages/CategoryList/categories';
import { POIList } from '../pages/POIList/poi';
import { POIDetail } from '../pages/POIDetail/poiDetail';
import { BuildingMap } from '../pages/BuildingMap/buildingMap';
import { MapDisplay } from '../pages/mapDisplay/map';
import { TabsPage } from '../pages/Tabs/tabs';
import { SelectionPage } from '../pages/selection/selection';
import { BlePage } from '../pages/ble/ble';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleMaps, Spherical } from '@ionic-native/google-maps';
import { DeviceOrientation } from '@ionic-native/device-orientation';
import { IBeacon } from '@ionic-native/ibeacon';
import { Device } from '@ionic-native/device';
import { Geolocation } from '@ionic-native/geolocation';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Deeplinks } from '@ionic-native/deeplinks';
import { LocalNotifications } from '@ionic-native/local-notifications';

@NgModule({
  declarations: [
    MyApp,
    BuildingListPage,
    ListPage,
    BuildingDashboard,
    PromoList,
    PromoDetail,
    CategoryList,
    POIList,
    POIDetail,
    BuildingMap,
    MapDisplay,
    TabsPage,
    SelectionPage,
    BlePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BuildingListPage,
    ListPage,
    BuildingDashboard,
    PromoList,
    PromoDetail,
    CategoryList,
    POIList,
    POIDetail,
    BuildingMap,
    MapDisplay,
    TabsPage,
    SelectionPage,
    BlePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GoogleMaps,
    Diagnostic,
    PermissionsService,
    Spherical,
    DeviceOrientation,
    IBeacon,
    Device,
    Geolocation,
    SocialSharing,
    Deeplinks,
    LocalNotifications,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
