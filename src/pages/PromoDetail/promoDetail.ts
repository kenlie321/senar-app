import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, Platform, LoadingController, ToastController } from 'ionic-angular';

import { BuildingMap } from '../BuildingMap/buildingMap';
import { POIDetail } from '../POIDetail/poiDetail';
import { MapDisplay } from '../mapDisplay/map';

declare var cordova:any;

BuildingMap
@Component({
  selector: 'promo-detail',
  templateUrl: 'promoDetail.html'
})

export class PromoDetail{
    promo:any;
    building:any;
    isEvent:any;
    isMall:any;
    poi:any;
    constructor(public navCtrl: NavController, public navParam:NavParams,
      public platform:Platform,public LoadingController:LoadingController,
      public ChangeDetectorRef:ChangeDetectorRef, public ToastController:ToastController){
      this.promo = this.navParam.get('promo');
      this.building = this.navParam.get('building');
      this.isEvent = this.navParam.get('isEvent');
      this.isMall = this.navParam.get('isMall');
      this.poi = this.navParam.get('poi');
    }

    ionViewWillEnter(){
      //this.platform.ready().then(() => {
        //console.log('awal',this.merchant);
        if(this.poi == undefined){
          this.fetchMerchant();
        }
      // }).catch(error => {
      //   console.log(error);
      // })
    }

    fetchMerchant(){
      // let loading = null;
      // if(!this.poi){
      //   loading = this.LoadingController.create({content: 'Loading POIs...'});
      //   loading.present();
      // }
      cordova.plugins.Situm.fetchIndoorPOIsFromBuilding(this.building, (result) => {
    		//console.log('indoor poi', result)
    		this.poi = result.find(value => {
          return value.poiName == this.promo.customFields.organizer;
        });
        //console.log('merchant',this.poi);
    		this.ChangeDetectorRef.detectChanges();
        //loading.dismiss();
    	}, (error) => {
    		const errorMessage = "An error occurred when recovering the Merchant/Organizer";
            console.log(`${errorMessage}`, error);
            //if(loading) loading.dismiss();
            this.presentToast(`${errorMessage} ${error}`, 'bottom', null);
    	})
    }

    panduButtonTapped(event){
      this.navCtrl.push(MapDisplay, {promo:this.promo, building:this.building, navigating: true});
    }

    merchantButtonTapped(event){
      this.navCtrl.push(POIDetail,{building:this.building,poi:this.poi,isEvent:this.isEvent,isMall:this.isMall})
    }

    presentToast(text, position, toastClass) {
      const toast = this.ToastController.create({
          message: text,
          duration: 3000,
          position: position,
          cssClass: toastClass ? toastClass : ''
      });
      toast.present();
  }
}