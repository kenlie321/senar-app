import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { 
    NavController,
    AlertController, 
    Platform, 
    ToastController, 
    LoadingController, 
    Slides,
    NavParams
} from 'ionic-angular';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    CameraPosition,
    MarkerOptions,
    Marker,
    MarkerCluster
} from '@ionic-native/google-maps';
import {USER_EMAIL, USER_API_KEY} from '../../services/situm';
import { TabsPage } from '../Tabs/tabs';
import { SelectionPage } from '../selection/selection';

declare var cordova: any;

@Component({
    selector: 'page-buildingList',
    templateUrl: 'buildingList.html'
})
export class BuildingListPage {
    @ViewChild(Slides) Slides: Slides;
    map: GoogleMap;

    public buildingList: any[] = [];
    public slidesCurrentIndex: number = 0;
    public listLength: number = 0;

    public unregisterBackButtonAction:any;

    isEvent:any;
    isMall:any;
    isLoading:boolean = false;

    constructor(
        public NavController: NavController,
        public AlertController: AlertController,
        public ToastController: ToastController,
        public LoadingController: LoadingController,
        public Platform: Platform,
        public ChangeDetectorRef: ChangeDetectorRef,
        public NavParams:NavParams
    ) {
        this.isEvent = this.NavParams.get('isEvent') == null ? false: this.NavParams.get('isEvent');
        this.isMall = this.NavParams.get('isMall') == null ? false : this.NavParams.get('isMall');
    }

    ionViewDidEnter() {
        this.Platform.ready().then(() => {
            cordova.plugins.Situm.setApiKey(USER_EMAIL, USER_API_KEY);
            cordova.plugins.Situm.setCacheMaxAge(300,(res) => {
                console.log(res);
            },error => {
                console.log(error);
            });
            this.fetchBuildingList();
        }).catch(error=> {
            console.log('error', error);
        });
        this.initializeBackButtonCustomHandler();
    }

    ionViewCanLeave(){
        return !this.isLoading;
    }

    ionViewWillLeave(){
        this.unregisterBackButtonAction && this.unregisterBackButtonAction();
    }

    initializeBackButtonCustomHandler(): void {
        this.unregisterBackButtonAction = this.Platform.registerBackButtonAction(() => {
            this.NavController.setRoot(SelectionPage);
        });
    }

    fetchBuildingList() {
        let loading = null;
        if(this.buildingList.length == 0) {
            loading = this.LoadingController.create({content: 'Loading building list...'});
            loading.present();
            this.isLoading = true;
        }

        cordova.plugins.Situm.fetchBuildings((result) => {
            console.log('result', result)
            if(this.isEvent){
                this.buildingList = result.filter((building) => {
                    return building.customFields.type == "event";
                });
            }else if(this.isMall){
                this.buildingList = result.filter((building) => {
                    return building.customFields.type == "mall";
                });
            }else{
                this.buildingList = result;
            }
            this.listLength = this.buildingList.length;
            this.slidesCurrentIndex = 1;
            if(loading){ 
                loading.dismiss();
                this.isLoading = false;
            }
            this.ChangeDetectorRef.detectChanges();
            this.loadMap();
        }, (error) => {
            const errorMessage = "An error occurred when recovering the buildings.";
            console.log(`${errorMessage}`, error);
            if(loading) {
                loading.dismiss();
                this.isLoading = false;
            }
            this.presentToast(`${errorMessage} ${error}`, 'bottom', null);
        })
    }

    presentToast(text, position, toastClass) {
        const toast = this.ToastController.create({
            message: text,
            duration: 3000,
            position: position,
            cssClass: toastClass ? toastClass : ''
        });
        toast.present();
    }

    slideChanged() {
        if(this.Slides.length() > 1){
            if((this.Slides.getActiveIndex()) == this.Slides.length()){
                return;
            }else{
                this.slidesCurrentIndex = this.Slides.getActiveIndex() + 1;
            }
        }
    }

    loadMap() {
        console.log("masuk");
        var target = [];
        var markerTarget = [];
        for (var i = 0; i < this.buildingList.length; i++) { 
            var center = {
                lat: this.buildingList[i].center.latitude,
                lng: this.buildingList[i].center.longitude,
            };
            var markerPosition = {
                position: {
                    lat: this.buildingList[i].center.latitude,
                    lng: this.buildingList[i].center.longitude,
                },label:"Location"
            }
            target.push(center);
            markerTarget.push(markerPosition);
        }
        let mapOptions: GoogleMapOptions = {
            camera: {
                target: target,
                zoom: 10,
                tilt: 30
            },
            controls: {
                'compass': true,
                'indoorPicker': false,
            },
        };
        this.map = GoogleMaps.create('map_canvas', mapOptions);
        //console.log("masuk 2");
        this.map.addMarkerCluster({
            markers: markerTarget,
            icons: [
                {min: 1, max: 100, url: "red", anchor: {x: 16, y: 16}, label:{bold:true,color:"black",fontSize:15}}
            ]
        }).then((MarkerCluster) => {
            MarkerCluster.on(GoogleMapsEvent.MARKER_CLICK).subscribe((cluster: any) => {
                console.log(cluster[0])
            });
        });
        //console.log(target);
        //console.log(markerTarget);
    }

    slideTapped(event, building){
        this.NavController.push(TabsPage, {building:building, isEvent:this.isEvent, isMall:this.isMall});
    }

    cek(){
        console.log(this.isEvent);
        console.log(this.isMall);
        console.log(this.buildingList);
    }

}
